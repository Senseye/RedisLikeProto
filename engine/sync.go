package engine

type (
	SyncEngineJob struct {
		request  string
		response chan string
	}

	SyncEngine struct {
		source Engine
		jobs   chan SyncEngineJob
	}
)

func (e *SyncEngine) Execute(s string) string {
	job := SyncEngineJob{
		request:  s,
		response: make(chan string),
	}

	e.jobs <- job

	return <-job.response
}

func Sync(source Engine, size int) Engine {
	result := &SyncEngine{
		source: source,
		jobs:   make(chan SyncEngineJob, size),
	}

	go result.worker()

	return result
}

func (e *SyncEngine) worker() {
	for job := range e.jobs {
		job.response <- e.source.Execute(job.request)
	}
}
