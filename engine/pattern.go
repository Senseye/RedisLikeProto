package engine

import (
	"regexp"
	"strings"
)

type (
	Pattern interface {
		Search(start *Key) []string
	}

	HasPrefixPattern struct {
		prefix string
	}

	HasSuffixPattern struct {
		suffix string
	}

	EqualPattern struct {
		search string
	}

	EmptyPattern struct {
	}

	RegexPattern struct {
		regex *regexp.Regexp
	}
)

func PatternCompile(pattern string) Pattern {
	count := strings.Count(pattern, PatternAll)

	switch count {
	case 0:
		return &EqualPattern{pattern}
	case 1:
		index := strings.IndexByte(pattern, PatternAllByte)
		if index == 0 {
			return &HasSuffixPattern{pattern[1:]}
		}

		last := len(pattern) - 1
		if index == last {
			return &HasPrefixPattern{pattern[:last]}
		}
	}

	replace := strings.Replace(pattern, PatternAll, "(\\w+)", count)
	regex, err := regexp.Compile(replace)

	if err == nil {
		return &RegexPattern{regex}
	}

	return &EmptyPattern{}
}

func (p *RegexPattern) Search(start *Key) []string {
	result := make([]string, 0, 1)

	current := start
	for current != nil {
		name := current.Name()

		if p.regex.Match([]byte(name)) {
			result = append(result, name)
		}

		current = current.Next()
	}

	return result
}

func (p *EmptyPattern) Search(start *Key) []string {
	return nil
}

func (p *EqualPattern) Search(start *Key) []string {
	search := p.search

	current := start
	for current != nil {
		name := current.Name()

		if name == search {
			return []string{name}
		}

		current = current.Next()
	}

	return nil
}

func (p *HasPrefixPattern) Search(start *Key) []string {
	result := make([]string, 0, 1)

	current := start
	for current != nil {
		name := current.Name()

		if strings.HasPrefix(name, p.prefix) {
			result = append(result, name)
		}

		current = current.Next()
	}

	return result
}

func (p *HasSuffixPattern) Search(start *Key) []string {
	result := make([]string, 0, 1)

	current := start
	for current != nil {
		name := current.Name()

		if strings.HasSuffix(name, p.suffix) {
			result = append(result, name)
		}

		current = current.Next()
	}

	return result
}
