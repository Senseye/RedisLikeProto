package engine

import (
	"fmt"
	"strconv"
	"strings"
)

// commands state
const (
	Info = "info"
)

const (
	Set  = "set"
	Get  = "get"
	Del  = "del"
	Keys = "keys"
	Type = "type"

	ListRange     = "lrange"
	ListRightPush = "rpush"
	ListLength    = "llen"
	ListSet       = "lset"
)

const (
	PatternAllByte = '*'
	PatternAll     = "*"
)

const (
	InfoResult = "server is running"
	Success    = "success"
)

const (
	Empty = "(empty list or set)"
)

const (
	// original: ERR WRONGTYPE Operation against a key holding the wrong kind of value
	ErrWrongType = "Operation against a key holding the wrong kind of value"

	// original: ERR ERR wrong number of arguments for 'rpush' command
	ErrWrongNumberOfArguments = "wrong number of arguments"

	// original: ERR ERR index out of range
	ErrIndexOutOfRange = "index out of range"

	// original: ERR ERR no such key
	ErrNoSuchKey = "no such key"

	// original: ERR ERR value is not an integer or out of range
	ErrIndexIsNotInteger = "index is not an integer"
)

type (
	Engine interface {
		Execute(command string) string
	}

	MemoryEngine struct {
		keyValueMap map[string]string
		keyListMap  map[string][]string
		keySpace    *KeySpace
	}
)

func (e *MemoryEngine) Execute(s string) string {
	switch s {
	case Info:
		return e.info()
	}

	index := strings.IndexByte(s, ' ')

	if index > 0 {
		command := s[:index]
		args := s[index+1:]

		switch command {
		case Set:
			return e.set(args)
		case Get:
			return e.get(args)
		case Del:
			return e.del(args)
		case Keys:
			return e.keysByPattern(args)

		case ListRange:
			return e.listRange(args)
		case ListRightPush:
			return e.listRightPush(args)
		case ListLength:
			return e.listLength(args)
		case ListSet:
			return e.listSet(args)

		case Type:
			return e.keyType(args)
		}
	}

	return fmt.Sprintf(`Undefined command "%s"`, s)
}

func (e *MemoryEngine) info() string {
	return InfoResult
}

func (e *MemoryEngine) set(s string) string {
	index := strings.IndexByte(s, ' ')

	key := s[:index]
	value := s[index+1:]

	dataType := e.keySpace.Type(key)

	switch dataType {
	case TypeNone:
		e.keySpace.Add(key, TypeString)

		fallthrough
	case TypeString:
		e.keyValueMap[key] = value

		return Success
	}

	return ErrWrongType
}

func (e *MemoryEngine) get(key string) string {
	dataType := e.keySpace.Type(key)

	switch dataType {
	case TypeString:
		return e.keyValueMap[key]
	case TypeNone:
		return ""
	}

	return ErrWrongType
}

func (e *MemoryEngine) del(s string) string {
	keys := strings.Split(s, " ")

	for _, key := range keys {
		dataType := e.keySpace.Type(key)

		switch dataType {
		case TypeString:
			delete(e.keyValueMap, key)
		case TypeList:
			delete(e.keyListMap, key)
		case TypeNone:
			// all fine
			continue
		default:
			DebugErrorf(`undefined data type "%s"`, dataType)

			continue
		}

		e.keySpace.Del(key)
	}

	return Success
}

func (e *MemoryEngine) listLength(key string) string {
	dataType := e.keySpace.Type(key)

	switch dataType {
	case TypeList:
		return strconv.Itoa(len(e.keyListMap[key]))
	case TypeNone:
		return "0"
	}

	return ErrWrongType
}

func (e *MemoryEngine) listRange(s string) string {
	args := strings.Split(s, " ")

	if len(args) < 3 {
		return ErrWrongNumberOfArguments
	}

	key := args[0]

	dataType := e.keySpace.Type(key)

	switch dataType {
	case TypeList:
		return strings.Join(e.keyListMap[key], " ")
	case TypeNone:
		return Empty
	}

	return ErrWrongType
}

func (e *MemoryEngine) listSet(s string) string {
	args := strings.Split(s, " ")

	if len(args) != 3 {
		return ErrWrongNumberOfArguments
	}

	key := args[0]

	dataType := e.keySpace.Type(key)

	switch dataType {
	case TypeList:
		index, err := strconv.Atoi(args[1])

		if err != nil {
			return ErrIndexIsNotInteger
		}

		if index < 0 {
			return ErrIndexOutOfRange
		}

		list := e.keyListMap[key]

		if index >= len(list) {
			return ErrIndexOutOfRange
		}

		list[index] = args[2]

		e.keyListMap[key] = list

		return Success
	case TypeNone:
		return ErrNoSuchKey
	}

	return ErrWrongType
}

func (e *MemoryEngine) listRightPush(s string) string {
	args := strings.Split(s, " ")

	if len(args) < 2 {
		return ErrWrongNumberOfArguments
	}

	key := args[0]

	dataType := e.keySpace.Type(key)

	switch dataType {
	case TypeNone:
		e.keySpace.Add(key, TypeList)

		fallthrough
	case TypeList:
		e.keyListMap[key] = append(e.keyListMap[key], args[1:]...)

		return Success
	}

	return ErrWrongType
}

func (e *MemoryEngine) keysByPattern(pattern string) string {
	switch pattern {
	case PatternAll:
		return e.keySpace.String()
	}

	return strings.Join(PatternCompile(pattern).Search(e.keySpace.Start()), " ")
}

func (e *MemoryEngine) keyType(key string) string {
	return e.keySpace.Type(key)
}

func New(keyValueSize, keySpaceSize int) Engine {
	return &MemoryEngine{
		keyValueMap: make(map[string]string, keyValueSize),
		keyListMap:  make(map[string][]string, 0),
		keySpace:    NewKeySpace(keySpaceSize),
	}
}
