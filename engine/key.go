package engine

import (
	"strings"
)

const (
	TypeNone   = "none"
	TypeString = "string"
	TypeList   = "list"
)

type (
	KeySpace struct {
		start   *Key
		nameMap map[string]*Key
	}

	Key struct {
		name     string
		dataType string
		prev     *Key
		next     *Key
	}
)

func NewKeySpace(size int) *KeySpace {
	return &KeySpace{
		nameMap: make(map[string]*Key, size),
	}
}

func (ks *KeySpace) Add(name, dataType string) {
	if existsKey, ok := ks.nameMap[name]; ok {
		existsKey.dataType = dataType
	} else {
		key := &Key{
			name:     name,
			dataType: dataType,
			prev:     nil,
			next:     ks.start,
		}

		if ks.start != nil {
			ks.start.prev = key
		}

		ks.nameMap[name] = key
		ks.start = key
	}
}

func (ks *KeySpace) Del(name string) {
	if existsKey, ok := ks.nameMap[name]; ok {
		if ks.start == existsKey {
			ks.start = existsKey.next
		} else {
			existsKey.prev.next = existsKey.next
		}

		delete(ks.nameMap, name)
	}
}

func (ks *KeySpace) Type(name string) string {
	if existsKey, ok := ks.nameMap[name]; ok {
		return existsKey.dataType
	}

	return TypeNone
}

func (ks *KeySpace) Start() *Key {
	return ks.start
}

func (ks *KeySpace) String() string {
	result := make([]string, 0, len(ks.nameMap))

	current := ks.start
	for current != nil {
		result = append(result, current.Name())

		current = current.Next()
	}

	return strings.Join(result, " ")
}

func (k *Key) Next() *Key {
	return k.next
}

func (k *Key) Name() string {
	return k.name
}
