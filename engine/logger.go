package engine

import (
	"fmt"
)

func TI() string {
	return "\x1b[92m[INFO]\x1b[0m"
}

func TW() string {
	return "\x1b[33m[WARNING]\x1b[0m"
}

func TE() string {
	return "\x1b[91m[ERROR]\x1b[0m"
}

func DebugInfo(a ...interface{}) {
	fmt.Println(TI(), a)
}

func DebugWarning(a ...interface{}) {
	fmt.Println(TW(), a)
}

func DebugError(a ...interface{}) {
	fmt.Println(TE(), a)
}

func DebugErrorf(format string, a ...interface{}) {
	DebugError(fmt.Printf(format, a...))
}
