package engine

var defaultBuilder = &Builder{}

type (
	Builder struct {
		keyValueSize int
		keySpaceSize int
	}
)

func (b *Builder) SetKeyValueSize(size int) {
	b.keyValueSize = size
}

func (b *Builder) SetKeySpaceSize(size int) {
	b.keySpaceSize = size
}

func (b *Builder) Build() Engine {
	return New(b.keyValueSize, b.keySpaceSize)
}

func SetKeyValueSize(size int) {
	defaultBuilder.SetKeyValueSize(size)
}

func SetKeySpaceSize(size int) {
	defaultBuilder.SetKeySpaceSize(size)
}

func Build() Engine {
	return defaultBuilder.Build()
}
