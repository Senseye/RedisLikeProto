package engine

import (
	"github.com/stretchr/testify/assert"
	"strconv"
	"testing"
	"time"
)

func TestMemoryEngine_Execute_Info(t *testing.T) {
	testMemoryEngine_Execute_Info(t, Build())

	testMemoryEngine_Execute_Info(t, Sync(Build(), 1))
}

func testMemoryEngine_Execute_Info(t *testing.T, state Engine) {
	assert.Equal(t, InfoResult, state.Execute(Info))
}

func TestMemoryEngine_Execute_SetGetKeys(t *testing.T) {
	state := Build()

	assert.Equal(t, "", state.Execute("keys *"))

	assert.Equal(t, "", state.Execute("get key1"))
	assert.Equal(t, "", state.Execute("get key2"))

	assert.Equal(t, Success, state.Execute("set key1 value1"))
	assert.Equal(t, Success, state.Execute("set key2 value2"))
	assert.Equal(t, "value1", state.Execute("get key1"))
	assert.Equal(t, "value2", state.Execute("get key2"))

	assert.Equal(t, "key2 key1", state.Execute("keys *"))
}

func TestMemoryEngine_Execute_Type_None(t *testing.T) {
	state := Build()

	assert.Equal(t, TypeNone, state.Execute("type key1"))
}

func TestMemoryEngine_Execute_Type_String(t *testing.T) {
	state := Build()

	state.Execute("set key1 value1")

	assert.Equal(t, TypeString, state.Execute("type key1"))
}

func TestMemoryEngine_Execute_Del(t *testing.T) {
	state := Build()

	state.Execute("set key1 value1")
	state.Execute("set key2 value2")
	state.Execute("set key3 value3")
	state.Execute("set key4 value4")

	assert.Equal(t, Success, state.Execute("del key0 key2 key3"))

	assert.Equal(t, "key4 key1", state.Execute("keys *"))
}

func TestMemoryEngine_Execute_Keys(t *testing.T) {
	state := Build()

	state.Execute("set k:a:1 1")
	state.Execute("set k:b:2 2")
	state.Execute("set k:b:3 3")

	assert.Equal(t, "k:b:3 k:b:2 k:a:1", state.Execute("keys *"))
	assert.Equal(t, "k:b:3 k:b:2 k:a:1", state.Execute("keys k:*"))

	assert.Equal(t, "k:b:3 k:b:2 k:a:1", state.Execute("keys k:*:*"))

	assert.Equal(t, "k:a:1", state.Execute("keys k:a:*"))
	assert.Equal(t, "k:b:3 k:b:2", state.Execute("keys k:b:*"))

	assert.Equal(t, "k:a:1", state.Execute("keys k:a:1"))
	assert.Equal(t, "k:a:1", state.Execute("keys *:1"))
	assert.Equal(t, "k:a:1", state.Execute("keys *:a:1"))

	assert.Equal(t, "k:b:2", state.Execute("keys *:2"))
	assert.Equal(t, "k:b:2", state.Execute("keys *:b:2"))

	assert.Equal(t, "k:b:3", state.Execute("keys *:3"))
	assert.Equal(t, "k:b:3", state.Execute("keys *:b:3"))
}

func TestMemoryEngine_Execute_List(t *testing.T) {
	state := Build()

	assert.Equal(t, Empty, state.Execute("lrange l:1 0 -1"))
	assert.Equal(t, "0", state.Execute("llen l:1"))

	assert.Equal(t, Success, state.Execute("rpush l:1 1"))
	assert.Equal(t, Success, state.Execute("rpush l:1 2"))
	assert.Equal(t, Success, state.Execute("rpush l:1 2"))
	assert.Equal(t, Success, state.Execute("rpush l:1 3 3 a"))

	assert.Equal(t, "1 2 2 3 3 a", state.Execute("lrange l:1 0 -1"))
	assert.Equal(t, "6", state.Execute("llen l:1"))
	assert.Equal(t, "l:1", state.Execute("keys *"))

	assert.Equal(t, Success, state.Execute("del l:1"))
	assert.Equal(t, "", state.Execute("keys *"))
	assert.Equal(t, "0", state.Execute("llen l:1"))

	// try add again and see new list
	assert.Equal(t, Success, state.Execute("rpush l:1 b"))
	assert.Equal(t, "b", state.Execute("lrange l:1 0 -1"))
	assert.Equal(t, "1", state.Execute("llen l:1"))
}

func TestMemoryEngine_Execute_List_Set(t *testing.T) {
	state := Build()

	assert.Equal(t, Success, state.Execute("rpush l:1 1"))
	assert.Equal(t, Success, state.Execute("rpush l:1 3"))
	assert.Equal(t, Success, state.Execute("rpush l:1 5"))

	assert.Equal(t, "1 3 5", state.Execute("lrange l:1 0 -1"))

	assert.Equal(t, Success, state.Execute("lset l:1 0 2"))
	assert.Equal(t, "2 3 5", state.Execute("lrange l:1 0 -1"))

	assert.Equal(t, Success, state.Execute("lset l:1 2 9"))
	assert.Equal(t, "2 3 9", state.Execute("lrange l:1 0 -1"))

	assert.Equal(t, ErrIndexOutOfRange, state.Execute("lset l:1 3 10"))
	assert.Equal(t, "2 3 9", state.Execute("lrange l:1 0 -1"))
}

func TestMemoryEngine_Execute_Undefined(t *testing.T) {
	state := Build()

	assert.Equal(t, `Undefined command "magic"`, state.Execute("magic"))
}

func BenchmarkEngine_Execute_SetNew(b *testing.B) {
	SetKeyValueSize(b.N)
	SetKeySpaceSize(b.N)

	state := Sync(Build(), 32)

	b.RunParallel(func(p *testing.PB) {
		for p.Next() {
			key := strconv.FormatInt(time.Now().UnixNano(), 10)

			state.Execute("set " + key + " " + key)
		}
	})
}
